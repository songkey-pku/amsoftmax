# -*- coding:utf-8 -*-
# 将图像数据生成lmdb数据集
# 1. 生成分类图像数据集
# 2. 生成目标检测图像数据集
import os
import sys
import numpy as np
import random
sys.path.append("../../python")
import caffe
from caffe.proto import caffe_pb2

import cv2
import lmdb

IMAGE_WIDTH = 90
IMAGE_HEIGHT = 90

file_lst=sys.argv[1]
lmdb_file = sys.argv[2]
batch_size = 256

if  os.path.exists(lmdb_file):
    os.system('rm -rf  ' + lmdb_file)




f = open(file_lst,"r")
lines = f.readlines()

key_id=[]
samples_name=[]
max_id=0
mk=0
for line in  lines:
    pos = line.rfind(' ')
    name_p = line[0:pos]
    id_p = int(line[pos + 1:])
    samples_name.append(name_p)
    key_id.append(id_p)

    if id_p>max_id:
        max_id=id_p
    mk = mk + 1
    if mk % 10000 == 0:
        print mk

    #print name_p

numSample=len(samples_name)
random_order = np.random.permutation(numSample).tolist()



# create the leveldb file
lmdb_env = lmdb.open(lmdb_file, map_size=int(1e12))#生成一个数据文件，定义最大空间
lmdb_txn = lmdb_env.begin(write=True)              #打开数据库的句柄
datum = caffe_pb2.Datum()                          #这是caffe中定义数据的重要类型



for count in range(numSample):
    idx = random_order[count]
    ful_name=samples_name[idx]
    #print ful_name
    img = cv2.imread(ful_name)
    if img.shape[0]!=IMAGE_HEIGHT:
        img=cv2.resize(img, (IMAGE_HEIGHT, IMAGE_WIDTH))
    height = img.shape[0]
    width = img.shape[1]
    channels = img.shape[2]
    #data = img.astype('int').transpose(2, 0, 1)
    data = img.astype('uint8').transpose(2, 0, 1)
    label = key_id[idx]
    datum = caffe.io.array_to_datum(data, label)

    keystr = '{:0>10d}'.format(count)
    lmdb_txn.put(keystr, datum.SerializeToString()) # 调用句柄，写入内存。
    if count % batch_size == 0:
        lmdb_txn.commit()
        lmdb_txn = lmdb_env.begin(write=True)
    if count %500==0:
        print 'batch {} writen'.format(count)

lmdb_txn.commit()
lmdb_txn = lmdb_env.begin(write=True)
lmdb_env.close()
