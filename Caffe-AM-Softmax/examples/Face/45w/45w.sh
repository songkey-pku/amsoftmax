#!/usr/bin/env sh
set -e

    #-weights=models/Face/AMsoft_128/face_train_88__iter_44000.caffemodel  \
./build/tools/caffe train \
   -solver=examples/Face/45w/face_solver.prototxt --gpu 0,1,2,3,4,5,6,7  $@ 2>&1 | tee log_45w.txt
