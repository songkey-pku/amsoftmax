#!/usr/bin/env sh
set -e

    #-weights=models/Face/AMsoft_128/face_train_88__iter_44000.caffemodel  \
./build/tools/caffe train \
   -weights=models/Face/AMsoft_128/face_88__iter_80000.caffemodel  \
   -solver=examples/Face/AMsoft_128/face_solver.prototxt --gpu 3,4  $@ 2>&1 | tee log_AMsoft_128_test.txt
