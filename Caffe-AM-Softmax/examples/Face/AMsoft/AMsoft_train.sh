#!/usr/bin/env sh
set -e
./build/tools/caffe train \
    -weights=models/Face/AMsoft/face_train_88__iter_44000.caffemodel  \ 
   -solver=examples/Face/AMsoft/face_solver.prototxt --gpu 0  $@ 2>&1 | tee log_AMsoft_test.txt
