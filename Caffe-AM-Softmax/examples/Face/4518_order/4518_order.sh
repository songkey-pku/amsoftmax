#!/usr/bin/env sh
set -e

    #-weights=models/Face/AMsoft_128/face_train_88__iter_44000.caffemodel  \
./build/tools/caffe train \
   -solver=examples/Face/4518_order/face_solver.prototxt --gpu 6,7  $@ 2>&1 | tee log_4518_order.txt
