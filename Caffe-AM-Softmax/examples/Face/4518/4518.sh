#!/usr/bin/env sh
set -e

    #-weights=models/Face/AMsoft_128/face_train_88__iter_44000.caffemodel  \
./build/tools/caffe train \
   -solver=examples/Face/4518/face_solver.prototxt --gpu 0,1  $@ 2>&1 | tee log_4518.txt
