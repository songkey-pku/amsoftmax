#!/usr/bin/env sh
set -e

    #-weights=models/Face/AMsoft_128/face_train_88__iter_44000.caffemodel  \
./build/tools/caffe train \
   -solver=examples/Face/85867/face_solver.prototxt --gpu 4,5  $@ 2>&1 | tee log_85867.txt
