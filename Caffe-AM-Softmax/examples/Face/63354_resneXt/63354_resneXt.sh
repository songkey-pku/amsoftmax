#!/usr/bin/env sh
set -e
#-weights=models/Face/63354_resneXt/face_63354-90_iter_30000_small_v2.caffemodel \
    #-weights=models/Face/AMsoft_128/face_train_88__iter_44000.caffemodel  \
./build/tools/caffe train \
   -solver=examples/Face/63354_resneXt/face_solver.prototxt --gpu 2,3  $@ 2>&1 | tee log_63354_resneXt.txt
