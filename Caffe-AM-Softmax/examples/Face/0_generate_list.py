'''
usage: python getImgListWithLabelCoral.py srcDirPath saveCategoryPath saveImgListPath
'''



#  /Users/allen/work/tensorflow_models/momo_face1000   /Users/allen/work/tensorflow_models/momo_face1000/Id.list /Users/allen/work/tensorflow_models/momo_face1000/img.list 4

'''
usage: python getImgListWithLabelCoral.py srcDirPath saveCategoryPath saveImgListPath imgNumThresh
'''
#coding=utf-8
import sys
import os
path = sys.argv[1]   # srcDirPath
#pathDirList = sys.argv[2] #saveDirListPath
pathImgList =  sys.argv[2]  #saveImgListPath
imgNumThresh_low = int(sys.argv[3])
imgNumThresh_high = 100000000
if imgNumThresh_high == 'inf':
    imgNumThresh_high = sys.maxint
else:
    imgNumThresh_high = int(imgNumThresh_high)
#fileType = sys.argv[6].split('|')
fileType = ['.jpg','.jpeg', '.png', '.bmp', '.jpg', 'JPG', '.JPEG', '.PNG', '.BMP']
#fileType = ['.5pt']
#fileType = ['.5pt-mtcnn']
# fileType = ['.feature']


def checkType(file_path, file_types):
    res = False
    f = file_path
    for file_type in file_types:
        if f.endswith(file_type):
            res = True
            break

    return res

#fDirList = open(pathDirList,'w')
fImgList = open(pathImgList,'w')
catNum = 0
for root,dir,file in os.walk(path):
    imageList = [] 
    for f in file:
        file_path = os.path.join(root,f)
        if checkType(file_path, fileType):
            imageList.append(file_path)
        #if countImg >= imgNumThresh:
        #    catNum +=1
        #    print(str(catNum))
        #    break
    imageNum = len(imageList)
    if imageNum >= imgNumThresh_low and imageNum <= imgNumThresh_high:
 #       fDirList.write(root+'\n')
        for imagePath in imageList:
            fImgList.write(imagePath+' '+ str(catNum)+'\n')
        catNum = catNum + 1

#fDirList.close()
fImgList.close()
print "Get image list ok!!"
