#include <functional>
#include <utility>
#include <vector>

#include "caffe/layers/balance_roc_layer.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

template <typename Dtype>
void BalanceRocLayer<Dtype>::LayerSetUp(
  const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {
  test_image_num_ = this->layer_param_.balance_roc_param().test_image_number();
        test_batch_size_ = this->layer_param_.balance_roc_param().test_batch_size();
        test_iter_ = this->layer_param_.balance_roc_param().test_iter();
        precision_ = this->layer_param_.balance_roc_param().precision();
        roc_iter_ = this->layer_param_.balance_roc_param().roc_iter();
        feature_dim_ = bottom[0]->channels() * bottom[0]->width() * bottom[0]->height();
        image_counter_ = 0;
        batch_counter_ = 0;
        test_counter_ = 0;

        CHECK_GT(precision_, 0);
        CHECK_GT(roc_iter_, 0);
        //CHECK_EQ(bottom[0]->num(), test_batch_size_);
        CHECK_EQ(bottom[0]->num(), bottom[1]->num());
        //CHECK_EQ(test_image_num_, test_batch_size_ * test_iter_);

}


template <typename Dtype>
    inline DistObject<Dtype> Calc_dist(int feature_dim, int label1, int label1_inner, std::vector<Dtype> vec1, int label2, int label2_inner, std::vector<Dtype> vec2){
        DistObject<Dtype> distObject;
        distObject.label1 = label1;
        distObject.label1_inner = label1_inner;
        distObject.label2 = label2;
        distObject.label2_inner = label2_inner;
        distObject.score = caffe_cpu_dot(feature_dim, vec1.data(), vec2.data());
        return distObject;
    }

  template <typename Dtype>
    inline Dtype Calc_dist(int feature_dim, const std::vector<Dtype> &vec1, const std::vector<Dtype> &vec2){
        return caffe_cpu_dot(feature_dim, vec1.data(), vec2.data());
    }

template <typename Dtype>
void BalanceRocLayer<Dtype>::Reshape(
  const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {
  vector<int> top_shape(0);  // Accuracy is a scalar; 0 axes.
        top[0]->Reshape(top_shape);
}


template <typename Dtype>
    void BalanceRocLayer<Dtype>::Norm(int feature_dim, const Dtype *src, std::vector<Dtype> &dst){
        Dtype num = caffe_cpu_dot(feature_dim, src, src);
        caffe_cpu_axpby<Dtype>(feature_dim, 1 / sqrt(num), src, 0, dst.data());
    }



template <typename Dtype>
void BalanceRocLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
    const vector<Blob<Dtype>*>& top) {
        const int count = bottom[0]->count();
        const int num = bottom[0]->num();
        //LOG(INFO) << "BalanceRoc num " << num;
        test_counter_++;
        if (image_counter_ < test_image_num_)
        {
            CHECK_EQ(feature_dim_, bottom[0]->channels() * bottom[0]->width() * bottom[0]->height());
            for (int i = 0; i < num; i++)
            {
                int label = (int)bottom[1]->cpu_data()[i];
                std::vector<Dtype> subVec(bottom[0]->cpu_data() + feature_dim_ * i, bottom[0]->cpu_data() + feature_dim_ * i + feature_dim_);
                //std::vector<Dtype> subVec(feature_dim_);
                //caffe_copy(feature_dim_, bottom[0]->cpu_data() + feature_dim_ * i, subVec.data());
                //Norm(feature_dim_, bottom[0]->cpu_data() + feature_dim_ * i, subVec);
                //LOG(INFO) << subVec[0];
                //LOG(INFO) << subVec[1];
                //LOG(INFO) << subVec[2];
                //LOG(INFO) << subVec[3];
                //LOG(INFO) << "BalanceRoc label = " << label;
                label_feature_map_[label].push_back(subVec);
                image_counter_++;
                if (image_counter_ >= test_image_num_)
                {
                    LOG(INFO) << "image_counter " << image_counter_ << " Finished. feature dim " << feature_dim_;
                    break;
                }
            }
            batch_counter_++;
            //LOG(INFO) << "Test Batch: " << batch_counter_;
            if (image_counter_ >= test_image_num_){
                if (image_counter_ % test_image_num_ == 0)
                {
                    //debug info
                    typename std::map<int, std::vector< std::vector<Dtype> > >::iterator it_end = label_feature_map_.end();
                    typename std::map<int, std::vector< std::vector<Dtype> > >::iterator it_begin = label_feature_map_.begin();
                    typename std::map<int, std::vector< std::vector<Dtype> > >::iterator it;

                    //LOG(INFO) << "DEBUG ##########";
                    //for (it = it_begin; it != it_end; it++)
                    //{
                    //    LOG(INFO) << it->first << " " << it->second.size();
                    //}

                    top[0]->mutable_cpu_data()[0] = Calc_roc();
                }
                //test_iter_++;
                //if (++test_iter_ % roc_iter_ == 0){
                //    top[0]->mutable_cpu_data()[0] = Calc_roc() * test_iter_;

                //    TPR_.clear();
                //    FPR_.clear();
                //}
                //else{
                //    top[0]->mutable_cpu_data()[0] = Calc_accuracy() * test_iter_;
                //}
                inter_class_dist_.clear();
                intra_class_dist_.clear();
                label_feature_map_.clear();
                //image_counter_ = 0;
                batch_counter_ = 0;
            }
        }
        else
        {
            if (test_counter_ >= test_iter_)
            {
                LOG(INFO) << "Reset ROC";
                test_counter_ = 0;
                image_counter_ = 0;
            }
        }
    }



template <typename Dtype>
    Dtype BalanceRocLayer<Dtype>::Calc_roc() {
        //LOG(INFO) << "Calc_roc() Begin...";
        typename std::map<int, std::vector< std::vector<Dtype> > >::iterator it_end = label_feature_map_.end();
        typename std::map<int, std::vector< std::vector<Dtype> > >::iterator it_begin = label_feature_map_.begin();
        typename std::map<int, std::vector< std::vector<Dtype> > >::iterator it;

        LOG(INFO) << "Calc dist begin:";
        LOG(INFO) << "Total: " << label_feature_map_.size() << " classes";
        std::vector<DistObject<Dtype> > allPairs;
        int dist_counter = 0;
        size_t inner_size = 0, intra_size = 0;
        for (it = it_begin; it != it_end; it++)
        {
            //LOG(INFO) << "it->second.size(): " << it->second.size();
            for (int i = 0; i < it->second.size(); i++)
            {
                // inter class
                if (it->second.size() > 1)
                {
                    for (int j = i + 1; j < it->second.size(); j++)
                    {
                        DistObject<Dtype> distObject;
                        distObject.isInner = true;
                        distObject.data1 = &it->second[i];
                        distObject.data2 = &it->second[j];
                        distObject.index = inner_size++;
                        allPairs.push_back(distObject);
                        //DistObject<Dtype> tmp_val = Calc_dist(feature_dim_, it->first, i, it->second[i], it->first, j, it->second[j]);
                        ////LOG(INFO) << "tmp_val:" << tmp_val;
                        //inter_class_dist_.push_back(tmp_val);
                    }
                }

                //intra class
                typename std::map<int, std::vector< std::vector<Dtype> > >::iterator tmp_it = it;
                if (++tmp_it == it_end) continue;
                for (; tmp_it != it_end; tmp_it++)
                {
                    for (int j = 0; j < tmp_it->second.size(); j++)
                    {
                        DistObject<Dtype> distObject;
                        distObject.isInner = false;
                        distObject.data1 = &it->second[i];
                        distObject.data2 = &tmp_it->second[j];
                        distObject.index = intra_size++;
                        allPairs.push_back(distObject);
                        //DistObject<Dtype> tmp_val = Calc_dist(feature_dim_, it->first, i, it->second[i], tmp_it->first, j, tmp_it->second[j]);
                        //intra_class_dist_.push_back(tmp_val);
                    }
                }
            }
            //LOG(INFO) << "class: " << dist_counter++ << "/" << label_feature_map_.size();
        }
        inter_class_dist_.resize(inner_size);
        intra_class_dist_.resize(intra_size);
       // tbb::parallel_for(size_t(0), size_t(allPairs.size()), [&](size_t i) {
        for (int i = 0; i < allPairs.size(); i++){
            allPairs[i].score = Calc_dist(feature_dim_, *allPairs[i].data1, *allPairs[i].data2);
            if (allPairs[i].isInner)
            {
                inter_class_dist_[allPairs[i].index] = allPairs[i];
            }
            else
            {
                intra_class_dist_[allPairs[i].index] = allPairs[i];
            }
        }
        //);
        LOG(INFO) << "Calc dist finished! inter size(" << inter_class_dist_.size() << ") intra size(" << intra_class_dist_.size() << ") total(" << inter_class_dist_.size() + intra_class_dist_.size() << ")";
        std::sort(inter_class_dist_.begin(), inter_class_dist_.end());
        std::sort(intra_class_dist_.begin(), intra_class_dist_.end());
        //std::ofstream fout("D:/FaceRec/caffe_train/tmp_intra.txt");
        //for (auto object : intra_class_dist_)
        //{
        //    fout << object.score << " " << object.label1 << " " << object.label1_inner << " " << object.label2 << " " << object.label2_inner << std::endl;
        //}
        //fout.close();
        LOG(INFO) << "inter " << inter_class_dist_.front().score << " ~ " << inter_class_dist_.back().score;
        LOG(INFO) << "intra " << intra_class_dist_.front().score << " ~ " << intra_class_dist_.back().score;
        Dtype th_1k = 0, th_1w = 0, th_10w = 0;
        size_t index_1k = intra_class_dist_.size() - 1 - intra_class_dist_.size() / 1000;
        size_t index_1w = intra_class_dist_.size() - 1 - intra_class_dist_.size() / 10000;
        size_t index_10w = intra_class_dist_.size() - 1 - intra_class_dist_.size() / 100000;
        th_1k = intra_class_dist_[index_1k].score;
        th_1w = intra_class_dist_[index_1w].score;
        th_10w = intra_class_dist_[index_10w].score;
        size_t index_tp = 0;
        for (index_tp = 0; index_tp < inter_class_dist_.size(); index_tp++)
        {
            if (inter_class_dist_[index_tp].score >= th_1k)
                break;
        }
        LOG(INFO) << "1k index in intra class = " << index_1k << " TH = " << th_1k << " TP = " << Dtype(inter_class_dist_.size() - index_tp) / inter_class_dist_.size();
        for (index_tp = 0; index_tp < inter_class_dist_.size(); index_tp++)
        {
            if (inter_class_dist_[index_tp].score >= th_1w)
                break;
        }
        LOG(INFO) << "1w index in intra class = " << index_1w << " TH = " << th_1w << " TP = " << Dtype(inter_class_dist_.size() - index_tp) / inter_class_dist_.size();
        for (index_tp = 0; index_tp < inter_class_dist_.size(); index_tp++)
        {
            if (inter_class_dist_[index_tp].score >= th_10w)
                break;
        }
        LOG(INFO) << "10w index in intra class = " << index_10w << " TH = " << th_10w << " TP = " << Dtype(inter_class_dist_.size() - index_tp) / inter_class_dist_.size();
        Dtype accuracy = 0;
        return accuracy;
    }

INSTANTIATE_CLASS(BalanceRocLayer);
REGISTER_LAYER_CLASS(BalanceRoc);

}  // namespace caffe
